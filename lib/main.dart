import 'package:flutter/material.dart';
import 'package:qr/qr.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: Container(
            height: 256,
            width: 256,
            child: CustomPaint(painter: QRPainter()),
          ),
        ),
      ),
    );
  }
}

class QRPainter extends CustomPainter {
  void _drawCircle(Canvas canvas, Paint paint, double x, double y, double size){
    canvas.drawOval(
      Rect.fromLTWH(x, y, size, size),
      paint
    );
  }

  void _drawCross(Canvas canvas, Paint paint, double x, double y, double size){
    canvas.drawLine(Offset(x,y), Offset(x + size,y + size), paint);
    canvas.drawLine(Offset(x + size,y), Offset(x,y + size), paint);
  }
  @override
  void paint(Canvas canvas, Size size){
    final qrCode = new QrCode(1, QrErrorCorrectLevel.L);
    qrCode.addData("Eda Faggot!");
    qrCode.make();
    final dotPaint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.black;
    final crossPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 3
      ..color = Colors.black;
    double pixelSize = size.width / 21;
    for(double i = 0; i < 256; i += pixelSize){
      for(double j = 0; j < 256; j += pixelSize){
        int a = (i/pixelSize).round();
        int b = (j/pixelSize).round();
        if(qrCode.isDark(a, b)){
          ((a+b)*a).isEven ? _drawCircle(canvas, dotPaint, i, j, pixelSize) : _drawCross(canvas, crossPaint, i, j, pixelSize);
//          _drawCross(canvas, crossPaint, i, j, pixelSize);
//          _drawCircle(canvas, crossPaint, i, j, pixelSize);
//          (a+b).isEven ? _drawCircle(canvas, dotPaint, i, j, pixelSize) : _drawCross(canvas, crossPaint, i, j, pixelSize);
//          _drawCircle(canvas, dotPaint, i, j, pixelSize);
        }
      }
    }
  }

  @override
  bool shouldRepaint(QRPainter oldDelegate) => false;
}